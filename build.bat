@echo off
cl /c /W3 /WX /Zi /DEBUG /I "\enet-1.3.13\include" /I "\SDL2-2.0.5\include" "src\build.cpp"
if %errorlevel% neq 0 exit /b %errorlevel%
echo.
link /DYNAMICBASE:NO /DEBUG /OUT:liero.exe build.obj "\SDL2-2.0.5\lib\x86\SDL2.lib" "\enet-1.3.13\enet.lib" winmm.lib ws2_32.lib
