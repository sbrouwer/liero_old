/* Copyright 2017, Stijn Brouwer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "liero_net.h"


#include <math.h>

int clamp(int a, int min, int max) {
    if (a < min) return min;
    if (a > max) return max;
    return a;
}

float clamp(float a, float min, float max) {
    if (a < min) return min;
    if (a > max) return max;
    return a;
}

float mix(float a, float b, float t) {
    return a + (b - a) * t;
}

float smin(float a, float b) {
    float k = 1.0f;
    float h = clamp( 0.5f+0.5f*(b-a)/k, 0.0f, 1.0f );
    return mix( b, a, h ) - k*h*(1.0f-h);
}

void applyTerrainMakeHole(ServerData *data, TerrainMakeHolePayload *hole) {
    float halfTexelSize = hole->radius * 2;
    int centerX = (int) hole->x,
        centerY = (int) hole->y;
    int offset = (int) ceilf(halfTexelSize);
    /* @ToDo optimize */
    int minX = centerX - offset,
        minY = centerY - offset;
    int maxX = centerX + offset,
        maxY = centerY + offset;
    int widthChunks = data->terrain.widthChunks,
        heightChunks = data->terrain.heightChunks;
    int minChunkX = clamp(minX / chunkSize, 0, widthChunks),
        minChunkY = clamp(minY / chunkSize, 0, heightChunks);
    int maxChunkX = clamp(maxX / chunkSize, 0, widthChunks - 1),
        maxChunkY = clamp(maxY / chunkSize, 0, heightChunks - 1);
    for (int chunkY = minChunkY; chunkY <= maxChunkY; ++chunkY) {
        for (int chunkX = minChunkX; chunkX <= maxChunkX; ++chunkX) {
            TerrainUpdatePayload chunkUpdate;
            chunkUpdate.chunkX = chunkX;
            chunkUpdate.chunkY = chunkY;
            /* If we're gonna update the chunk, why not just update the whole chunk for simplicity's sake */
            for (int y = 0; y < chunkSize; ++y) {
                for (int x = 0; x < chunkSize; ++x) {
                    int texelX = chunkX * chunkSize + x,
                        texelY = chunkY * chunkSize + y;
                    float deltaX = (float) texelX - hole->x,
                          deltaY = (float) texelY - hole->y;
                    float distanceFieldValue = sqrtf(deltaX * deltaX + deltaY * deltaY) - hole->radius;
                    int updateIndex = x + y * chunkSize,
                        terrainIndex = (x + chunkX * chunkSize) + (y + chunkY * chunkSize) * data->terrain.widthTexels;
                    chunkUpdate.texels[updateIndex] =
                        data->terrain.texels[terrainIndex] =
                        smin(data->terrain.texels[terrainIndex], distanceFieldValue);
                }
            }
            push(&data->queuedTerrainUpdates, chunkUpdate);
        }
    }
}

void applyTerrainUpdate(ClientData *data, TerrainUpdatePayload *update) {
    int cx = update->chunkX * chunkSize,
        cy = update->chunkY * chunkSize;
    for (int y = 0; y < chunkSize; ++y) {
        float *source = &update->texels[y * chunkSize];
        float *destination = &data->terrain.texels[cx + (y + cy) * data->terrain.widthTexels];
        memcpy(destination, source, chunkSize * sizeof(float));
    }
    data->terrain.chunks[update->chunkX + update->chunkY * data->terrain.widthChunks] |= CHUNK_BITS_DIRTY;
}
