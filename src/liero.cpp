/* Copyright 2017, Stijn Brouwer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "liero.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

void die(const char *fmt, ...) {
    static char buf[512];
    va_list args;
    va_start(args, fmt);
    vsnprintf(buf, sizeof(buf), fmt, args);
    va_end(args);
    fprintf(stderr, "Fatal error:\n%s\n", buf);
    exit(EXIT_FAILURE);
}

void *xmalloc(size_t size) {
    void *mem = malloc(size);
    if (!mem) {
        DIE_OOM();
    }
    return mem;
}

void *xzalloc(size_t size) {
    void *mem = calloc(1, size);
    if (!mem) {
        DIE_OOM();
    }
    return mem;
}

void *xrealloc(void *ptr, size_t size) {
    void *mem = realloc(ptr, size);
    if (!mem) {
        DIE_OOM();
    }
    return mem;
}

void marchSquares(Terrain *terrain, int chunkX, int chunkY) {
    CollisionGeometry *cgeo = &terrain->collisionGeometries[chunkX + chunkY * terrain->widthChunks];
    auto sample = [&](int x, int y) {
        return terrain->texels[x + y * terrain->widthTexels];
    };
    clear(cgeo);
    int cx = chunkX * chunkSize, cy = chunkY * chunkSize;
    for (int y = cy; y < chunkSize + cy; ++y) {
        for (int x = cx; x < chunkSize + cx; ++x) {
            // @Cleanliness dry this shit up
            float verts[4] = {
                sample(x, y),
                sample(x+1, y),
                sample(x+1, y+1),
                sample(x, y+1),
            };
            auto findAlpha = [&](int i, int j) {
                // @FixMe make this dependent on terrainIsovalue
                return verts[i] / (verts[i] - verts[j]);
            };
            int solidCount = 0;
            for (int i = 0; i < 4; ++i) {
                if (verts[i] >= terrainIsovalue) ++solidCount;
            }
            if (solidCount == 0 || solidCount == 4) {
                continue;
            }
            else if (solidCount == 1) {
                if (verts[0] >= terrainIsovalue) {
                    push(cgeo, Edge(Vector2((float) x + findAlpha(0, 1), (float) y),
                                    Vector2((float) x, (float) y + findAlpha(0, 3))));
                }
                else if (verts[1] >= terrainIsovalue) {
                    push(cgeo, Edge(Vector2((float) x + 1, (float) y + findAlpha(1, 2)),
                                    Vector2((float) x + findAlpha(0, 1), (float) y)));
                }
                else if (verts[2] >= terrainIsovalue) {
                    push(cgeo, Edge(Vector2((float) x + findAlpha(3, 2), (float) y + 1),
                                    Vector2((float) x + 1, (float) y + findAlpha(1, 2))));
                }
                else {
                    push(cgeo, Edge(Vector2((float) x, (float) y + findAlpha(0, 3)),
                                    Vector2((float) x + findAlpha(3, 2), (float) y + 1)));
                }
            }
            else if (solidCount == 2) {
                if (verts[0] >= terrainIsovalue && verts[1] >= terrainIsovalue) {
                    push(cgeo, Edge(Vector2((float) x + 1, (float) y + findAlpha(1, 2)),
                                    Vector2((float) x, (float) y + findAlpha(0, 3))));
                }
                else if (verts[2] >= terrainIsovalue && verts[3] >= terrainIsovalue) {
                    push(cgeo, Edge(Vector2((float) x, (float) y + findAlpha(0, 3)),
                                    Vector2((float) x + 1, (float) y + findAlpha(1, 2))));
                }

                else if (verts[0] >= terrainIsovalue && verts[3] >= terrainIsovalue) {
                    push(cgeo, Edge(Vector2((float) x + findAlpha(0, 1), (float) y),
                                    Vector2((float) x + findAlpha(3, 2), (float) y + 1)));
                }
                else if (verts[1] >= terrainIsovalue && verts[2] >= terrainIsovalue) {
                    push(cgeo, Edge(Vector2((float) x + findAlpha(3, 2), (float) y + 1),
                                    Vector2((float) x + findAlpha(0, 1), (float) y)));
                }

                else {
                    // Calculate the value at the center of the square using bilinear interpolation
                    float topMiddle = (verts[0] + verts[1]) / 2;
                    float bottomMiddle = (verts[2] + verts[3]) / 2;
                    float center = (topMiddle + bottomMiddle) / 2;
                    if (center >= terrainIsovalue) {
                        if (verts[0] >= terrainIsovalue && verts[2] >= terrainIsovalue) {
                            push(cgeo, Edge(Vector2((float) x + findAlpha(0, 1), (float) y),
                                            Vector2((float) x + 1, (float) y + findAlpha(1, 2))));
                            push(cgeo, Edge(Vector2((float) x + findAlpha(3, 2), (float) y + 1),
                                            Vector2((float) x, (float) y + findAlpha(0, 3))));
                        }
                        else {
                            push(cgeo, Edge(Vector2((float) x, (float) y + findAlpha(0, 3)),
                                            Vector2((float) x + findAlpha(0, 1), (float) y)));
                            push(cgeo, Edge(Vector2((float) x + 1, (float) y + findAlpha(1, 2)),
                                            Vector2((float) x + findAlpha(3, 2), (float) y + 1)));
                        }
                    }
                    else {
                        if (verts[0] >= terrainIsovalue && verts[2] >= terrainIsovalue) {
                            push(cgeo, Edge(Vector2((float) x + findAlpha(0, 1), (float) y),
                                            Vector2((float) x, (float) y + findAlpha(0, 3))));
                            push(cgeo, Edge(Vector2((float) x + findAlpha(3, 2), (float) y + 1),
                                            Vector2((float) x + 1, (float) y + findAlpha(1, 2))));
                        }
                        else {
                            push(cgeo, Edge(Vector2((float) x + 1, (float) y + findAlpha(1, 2)),
                                            Vector2((float) x + findAlpha(0, 1), (float) y)));
                            push(cgeo, Edge(Vector2((float) x, (float) y + findAlpha(0, 3)),
                                            Vector2((float) x + findAlpha(3, 2), (float) y + 1)));
                        }
                    }
                }
            }
            else if (solidCount == 3) {
                if (verts[0] < terrainIsovalue) {
                    push(cgeo, Edge(Vector2((float) x, (float) y + findAlpha(0, 3)),
                                    Vector2((float) x + findAlpha(0, 1), (float) y)));
                }
                else if (verts[1] < terrainIsovalue) {
                    push(cgeo, Edge(Vector2((float) x + findAlpha(0, 1), (float) y),
                                    Vector2((float) x + 1, (float) y + findAlpha(1, 2))));
                }
                else if (verts[2] < terrainIsovalue) {
                    push(cgeo, Edge(Vector2((float) x + 1, (float) y + findAlpha(1, 2)),
                                    Vector2((float) x + findAlpha(3, 2), (float) y + 1)));
                }
                else {
                    push(cgeo, Edge(Vector2((float) x + findAlpha(3, 2), (float) y + 1),
                                    Vector2((float) x, (float) y + findAlpha(0, 3))));
                }
            }
        }
    }
}

void terrainUpdateCGeos(Terrain *terrain) {
    // @Speed embarrasingly parallel
    for (int y = 0; y < terrain->heightChunks; ++y) {
        for (int x = 0; x < terrain->widthChunks; ++x) {
            if (terrain->chunks[x + y * terrain->widthChunks] & CHUNK_BIT_DIRTY_COLLISION) {
                marchSquares(terrain, x, y);
                terrain->chunks[x + y * terrain->widthChunks] &= ~CHUNK_BIT_DIRTY_COLLISION;
            }
        }
    }
}

void destroyTerrain(Terrain *terrain) {
    for (int i = 0; i < terrain->widthChunks * terrain->heightChunks; ++i) {
        destroyStack(&terrain->collisionGeometries[i]);
    }
    free(terrain->collisionGeometries);
    free(terrain->chunks);
    free(terrain->texels);
}

#include <math.h>

void createTestTerrain(Terrain *terrain) {
    terrain->widthTexels = 128;
    terrain->heightTexels = 128;
    terrain->widthChunks = (terrain->widthTexels - 1) / chunkSize;
    terrain->heightChunks = (terrain->heightTexels - 1) / chunkSize;
    int texelSurface = terrain->widthTexels * terrain->heightTexels,
        chunkSurface = terrain->widthChunks * terrain->heightChunks;
    terrain->texels = (float *) xmalloc(sizeof(float) * texelSurface);
    terrain->chunks = (ChunkBits *) xzalloc(sizeof(ChunkBits) * chunkSurface);
    terrain->collisionGeometries = (CollisionGeometry *) xzalloc(sizeof(CollisionGeometry) * chunkSurface);

    auto func = [](float x, float y) {
        /* modulo'd circles */
        float modx = fmodf(x, 2) - 1;
        float mody = fmodf(y, 2) - 1;
        return modx*modx + mody*mody - 0.5f;
    };

    float scale = (float) 1. / terrain->widthTexels * 2 * 4;
    for (int y = 0; y < terrain->heightTexels; ++y) {
        for (int x = 0; x < terrain->widthTexels; ++x) {
            /* checkerboard pattern */
            bool flipped = (x / 16 % 2 == 0) != (y / 16 % 2 == 0);
            terrain->texels[x + y * terrain->widthTexels] = func(x * scale, y * scale) * (flipped ? -1 : 1);
            terrain->texels[x + y * terrain->widthTexels] = terrainMaxDistance;
        }
    }

    for (int i = 0; i < chunkSurface; ++i) {
        createStack(&terrain->collisionGeometries[i], collisionGeometryDefaultCapacity);
        marchSquares(terrain, i % terrain->widthChunks, i / terrain->widthChunks);
    }
}
