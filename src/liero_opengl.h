/* Copyright 2017, Stijn Brouwer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LIERO_OPENGL_H_GUARD_
#define __LIERO_OPENGL_H_GUARD_

#include "liero.h"
#include "glcorearb.h"

/** OpenGL function pointer table */
struct GL {
    PFNGLDEBUGMESSAGECALLBACKPROC DebugMessageCallback;

    PFNGLENABLEPROC    Enable;
    PFNGLDISABLEPROC   Disable;
    PFNGLBLENDFUNCPROC BlendFunc;

    PFNGLCLEARCOLORPROC ClearColor;
    PFNGLCLEARPROC      Clear;

    PFNGLVIEWPORTPROC Viewport;

    PFNGLDRAWARRAYSPROC DrawArrays;

    PFNGLCREATESHADERPROC  CreateShader;
    PFNGLDELETESHADERPROC  DeleteShader;
    PFNGLSHADERSOURCEPROC  ShaderSource;
    PFNGLCOMPILESHADERPROC CompileShader;

    PFNGLCREATEPROGRAMPROC        CreateProgram;
    PFNGLDELETEPROGRAMPROC        DeleteProgram;
    PFNGLATTACHSHADERPROC         AttachShader;
    PFNGLBINDFRAGDATALOCATIONPROC BindFragDataLocation;
    PFNGLLINKPROGRAMPROC          LinkProgram;
    PFNGLUSEPROGRAMPROC           UseProgram;

    PFNGLUNIFORM1IPROC        Uniform1i;
    PFNGLUNIFORM2IPROC        Uniform2i;
    PFNGLUNIFORMMATRIX4FVPROC UniformMatrix4fv;

    PFNGLGETATTRIBLOCATIONPROC       GetAttribLocation;
    PFNGLENABLEVERTEXATTRIBARRAYPROC EnableVertexAttribArray;
    PFNGLENABLEVERTEXATTRIBARRAYPROC DisableVertexAttribArray;
    PFNGLVERTEXATTRIBPOINTERPROC     VertexAttribPointer;

    PFNGLGENVERTEXARRAYSPROC    GenVertexArrays;
    PFNGLDELETEVERTEXARRAYSPROC DeleteVertexArrays;
    PFNGLBINDVERTEXARRAYPROC    BindVertexArray;

    PFNGLGENBUFFERSPROC    GenBuffers;
    PFNGLDELETEBUFFERSPROC DeleteBuffers;
    PFNGLBINDBUFFERPROC    BindBuffer;
    PFNGLBUFFERDATAPROC    BufferData;
    PFNGLBUFFERSUBDATAPROC BufferSubData;

    PFNGLACTIVETEXTUREPROC  ActiveTexture;
    PFNGLGENTEXTURESPROC    GenTextures;
    PFNGLDELETETEXTURESPROC DeleteTextures;
    PFNGLBINDTEXTUREPROC    BindTexture;
    PFNGLTEXPARAMETERIPROC  TexParameteri;
    PFNGLTEXPARAMETERFVPROC TexParameterfv;
    PFNGLTEXIMAGE2DPROC     TexImage2D;
    PFNGLTEXSUBIMAGE2DPROC  TexSubImage2D;

    PFNGLGENFRAMEBUFFERSPROC      GenFramebuffers;
    PFNGLDELETEFRAMEBUFFERSPROC   DeleteFramebuffers;
    PFNGLBINDFRAMEBUFFERPROC      BindFramebuffer;
    PFNGLFRAMEBUFFERTEXTURE2DPROC FramebufferTexture2D;
};

/** Load OpenGL function pointers into a GL struct */
void loadGL(GL *gl);

/** Simple vertex type used for immediate mode graphics */
struct Vertex {
    float x, y, z;
    unsigned char r, g, b, a;
    float u, v;
};

/** Enables vertex attribs for Vertex, uses simpleVS's attrib locations */
void vtxEnableAttribs(GL *gl);
/** Disables the attribs enabled by vtxEnableAttribs */
void vtxDisableAttribs(GL *gl);

constexpr int immediateVBOCapacity = 1024 * 1024;
constexpr int immediateBufferDefaultCapacity = 4096;

/**
 * Graphics resource handles and related state
 * This is transient and it should be possible to recreate and hotswap this on the fly
 * (in order to enable hot code and data reloading).
 */
struct GfxState {
    /** Backbuffer resolution */
    int backbufferWidth, backbufferHeight;

    /** terrainCoverageTexture resolution */
    int coverageBufferWidth, coverageBufferHeight;

    /**
     * General VAO for things that aren't big and complex enough to need an actual VAO
     * (e.g. immediate mode debug drawing procedures and fullscreen quads)
     */
    GLuint openglSucksVAO;

    /** VBO for simple immediate mode rendering */
    GLuint immediateVBO;

    /** Client-side contents of the immediate mode vertex buffer */
    int immediateCapacity, immediateCount;
    Vertex *immediateBuffer;

    /** Currently set color for drawing immediate mode primitives with */
    unsigned char immediateColor[4];

    /** Simple vertex shader for use with immediate mode rendering */
    GLuint simpleVS;

    /** Shader that simply draws primitives using their vertex colors */
    GLuint simpleFS;
    GLuint simpleProgram;

    /** A R32F texture containing a facsimile of Terrain.texels */
    GLuint terrainValueTexture;

    /**
     * Framebuffer containing an (aliased and binary) coverage map of the terrain,
     * that we will later downsample for use as an anti-aliased alpha mask
     * for rendering the terrain.
     */
    GLuint terrainCoverageFBO;
    GLuint terrainCoverageTexture;

    /** Shader that produces a binary coverage map of the terrain */
    GLuint terrainCoverageFS;
    GLuint terrainCoverageProgram;

    /** Texture containing a tiled image to draw terrain with */
    GLuint terrainGraphicsTexture;

    /**
     * Shader that samples the coverage and graphics texture and produces
     * an anti-aliased image of the terrain
     */
    GLuint terrainGraphicsFS;
    GLuint terrainGraphicsProgram;

    /** Temporary buffer for gfxUpdateTerrain */
    float *terrainValueTextureSubImageBuffer;
};

/** Creates new graphics resources to use for rendering. */
void createGfxState(GL *gl, GfxState *gfx, int resolutionX, int resolutionY);
/** Releases the graphics resources. */
void destroyGfxState(GL *gl, GfxState *gfx);

/** Empties the immediate mode vertex buffer */
void immClear(GfxState *gfx);
/** Pushes a single vertex into the immediate mode vertex buffer */
void immVertex(GfxState *gfx, Vertex vert);
/**
 * Flushes the immediate mode vertex buffer to the GPU,
 * also sets the VBO to the immediate mode VBO for rendering the flushed primitives.
 * Returns the vertex count in the buffer.
 */
int immFlush(GL *gl, GfxState *gfx);

/** Set the color to draw with */
void immSetColor(GfxState *gfx, unsigned char r, unsigned char g, unsigned char b, unsigned char a);
void immSetColor(GfxState *gfx, unsigned char r, unsigned char g, unsigned char b);
/** Draw a single line segment */
void immEdge(GfxState *gfx, float x0, float y0, float x1, float y1);
/** Draw a rectangle */
void immQuad(GfxState *gfx, float x, float y, float hw, float hh, float z);
void immQuad(GfxState *gfx, float x, float y, float hw, float hh);
/** Draws a rectangle where the UV-coords at the edges are aligned to the texels */
void immTerrainQuad(GfxState *gfx, int width, int height);
/** Draw some text using stb_easy_font */
void immShittyText(GfxState *gfx, float x, float y, float scale, const char *text);
/** Visualize collision geometry */
void immCGeo(GfxState *gfx, CollisionGeometry *cgeo, bool normals = true);

/** Uploads the entire terrain onto the GPU */
void gfxUploadTerrain(GL *gl, GfxState *gfx, Terrain *terrain);
/** Uploads only the changed parts of the terrain to the GPU */
void gfxUpdateTerrain(GL *gl, GfxState *gfx, Terrain *terrain);

/** Draws the terrain as it currently exists in the GfxState */
void gfxDrawTerrain(GL *gl, GfxState *gfx);
/** Draws debug information for the terrain (like collision edges and normals) */
void gfxDebugDrawTerrain(GL *gl, GfxState *gfx, Terrain *terrain);

#endif
