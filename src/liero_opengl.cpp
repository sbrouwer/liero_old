/* Copyright 2017, Stijn Brouwer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "liero_opengl.h"
#include <stdio.h>
#include <SDL.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "stb_easy_font.h"

void loadGL(GL *gl) {
#define LOADPROC(field) gl->field = (decltype(gl->field)) SDL_GL_GetProcAddress("gl" #field)
    LOADPROC(DebugMessageCallback);
    LOADPROC(Enable);
    LOADPROC(Disable);
    LOADPROC(BlendFunc);
    LOADPROC(ClearColor);
    LOADPROC(Clear);
    LOADPROC(Viewport);
    LOADPROC(DrawArrays);
    LOADPROC(CreateShader);
    LOADPROC(DeleteShader);
    LOADPROC(ShaderSource);
    LOADPROC(CompileShader);
    LOADPROC(CreateProgram);
    LOADPROC(DeleteProgram);
    LOADPROC(AttachShader);
    LOADPROC(BindFragDataLocation);
    LOADPROC(LinkProgram);
    LOADPROC(UseProgram);
    LOADPROC(Uniform1i);
    LOADPROC(Uniform2i);
    LOADPROC(UniformMatrix4fv);
    LOADPROC(GetAttribLocation);
    LOADPROC(EnableVertexAttribArray);
    LOADPROC(DisableVertexAttribArray);
    LOADPROC(VertexAttribPointer);
    LOADPROC(GenVertexArrays);
    LOADPROC(DeleteVertexArrays);
    LOADPROC(BindVertexArray);
    LOADPROC(GenBuffers);
    LOADPROC(DeleteBuffers);
    LOADPROC(BindBuffer);
    LOADPROC(BufferData);
    LOADPROC(BufferSubData);
    LOADPROC(ActiveTexture);
    LOADPROC(GenTextures);
    LOADPROC(DeleteTextures);
    LOADPROC(BindTexture);
    LOADPROC(TexParameteri);
    LOADPROC(TexParameterfv);
    LOADPROC(TexImage2D);
    LOADPROC(TexSubImage2D);
    LOADPROC(GenFramebuffers);
    LOADPROC(DeleteFramebuffers);
    LOADPROC(BindFramebuffer);
    LOADPROC(FramebufferTexture2D);
#undef LOADPROC
}

const char *simpleVSSource = R"GLSL(
#version 330 core
#extension GL_ARB_explicit_uniform_location : require
layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_color;
layout(location = 2) in vec2 vertex_texCoord;
out vec3 fragment_color;
out vec2 fragment_texCoord;
layout(location = 0) uniform mat4 uniform_transform;
void main() {
    gl_Position = uniform_transform * vec4(vertex_position, 1.0);
    fragment_color = vertex_color;
    fragment_texCoord = vertex_texCoord;
}
)GLSL";

const char *simpleFSSource = R"GLSL(
#version 330 core
in vec3 fragment_color;
out vec4 output_color;
void main() {
    output_color = vec4(fragment_color, 1.0);
}
)GLSL";

const char *terrainCoverageFSSource = R"GLSL(
#version 330 core
#extension GL_ARB_explicit_uniform_location : require
in vec2 fragment_texCoord;
out float output_alpha;
layout (location = 1) uniform sampler2D uniform_values;
const float infinity = 1.0 / 0.0;
void main() {
    /* @Hack using some trickery with floating point infinities in order to avoid a manual branch
     * don't know if this is actually better in practice
     * @UndefinedBehavior in OpenGL <4.1 */
    output_alpha = texture(uniform_values, fragment_texCoord).r * infinity;
    /* This should be equivalent to this when also considering the clamping by the FBO texture:
     * output_alpha = texture(uniform_values, ...).r >= 0.0 ? 1.0 : 0.0; */
}
)GLSL";

const char *terrainGraphicsFSSource = R"GLSL(
#version 330 core
#extension GL_ARB_explicit_uniform_location : require
in vec2 fragment_texCoord;
out vec4 output_color;
layout (location = 1) uniform sampler2D uniform_color;
layout (location = 2) uniform sampler2D uniform_coverage;
layout (location = 3) uniform ivec2 uniform_viewportSize;
void main() {
    vec3 colorData = texture(uniform_color, fragment_texCoord).rgb;
    // @FixMe should use fragcoord, not texcoord
    vec2 screenSpaceTexCoord = vec2(gl_FragCoord.xy) / vec2(uniform_viewportSize);
    float coverage = texture(uniform_coverage, screenSpaceTexCoord).r;
    output_color = vec4(colorData, coverage);
}
)GLSL";

void vtxEnableAttribs(GL *gl) {
    gl->EnableVertexAttribArray(0);
    gl->EnableVertexAttribArray(1);
    gl->EnableVertexAttribArray(2);
    gl->VertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * 4, 0);
    gl->VertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_FALSE, 6 * 4, (void *) (3 * sizeof(float)));
    gl->VertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 6 * 4, (void *) (4 * sizeof(float)));
}

void vtxDisableAttribs(GL *gl) {
    gl->DisableVertexAttribArray(0);
    gl->DisableVertexAttribArray(1);
    gl->DisableVertexAttribArray(2);
}

void createShader(GL *gl, GLenum type, GLuint *handle, const char *source) {
    *handle = gl->CreateShader(type);
    gl->ShaderSource(*handle, 1, &source, nullptr);
    gl->CompileShader(*handle);
    /* @ToDo actual shader compilation error handling */
    char buf[512];
    ((PFNGLGETSHADERINFOLOGPROC) SDL_GL_GetProcAddress("glGetShaderInfoLog"))(*handle, 512, nullptr, buf);
    if (buf[0]) fprintf(stderr, "Shader compilation error: %s\n", buf);
}

void createGfxState(GL *gl, GfxState *gfx, int resolutionX, int resolutionY) {
    gfx->backbufferWidth = resolutionX;
    gfx->backbufferHeight = resolutionY;
    gfx->coverageBufferWidth = resolutionX * 2;
    gfx->coverageBufferHeight = resolutionY * 2;

    gfx->terrainValueTextureSubImageBuffer = (float *) xmalloc(sizeof(float) * chunkSize * chunkSize);

    gl->GenVertexArrays(1, &gfx->openglSucksVAO);
    gl->BindVertexArray(gfx->openglSucksVAO);

    gl->GenBuffers(1, &gfx->immediateVBO);
    gl->BindBuffer(GL_ARRAY_BUFFER, gfx->immediateVBO);
    gl->BufferData(GL_ARRAY_BUFFER, immediateVBOCapacity, nullptr, GL_DYNAMIC_DRAW);

    gfx->immediateCount = 0;
    gfx->immediateCapacity = immediateBufferDefaultCapacity;
    gfx->immediateBuffer = (Vertex *) xmalloc(gfx->immediateCapacity * sizeof(Vertex));

    createShader(gl, GL_VERTEX_SHADER, &gfx->simpleVS, simpleVSSource);
    createShader(gl, GL_FRAGMENT_SHADER, &gfx->simpleFS, simpleFSSource);

    gfx->simpleProgram = gl->CreateProgram();
    gl->AttachShader(gfx->simpleProgram, gfx->simpleVS);
    gl->AttachShader(gfx->simpleProgram, gfx->simpleFS);
    gl->BindFragDataLocation(gfx->simpleProgram, 0, "output_color");
    gl->LinkProgram(gfx->simpleProgram);

    createShader(gl, GL_FRAGMENT_SHADER, &gfx->terrainCoverageFS, terrainCoverageFSSource);

    gfx->terrainCoverageProgram = gl->CreateProgram();
    gl->AttachShader(gfx->terrainCoverageProgram, gfx->simpleVS);
    gl->AttachShader(gfx->terrainCoverageProgram, gfx->terrainCoverageFS);
    gl->BindFragDataLocation(gfx->terrainCoverageProgram, 0, "output_alpha");
    gl->LinkProgram(gfx->terrainCoverageProgram);

    gl->GenTextures(1, &gfx->terrainValueTexture);
    gl->BindTexture(GL_TEXTURE_2D, gfx->terrainValueTexture);
    gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    const float borderValue[] = { 0.5 };
    gl->TexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderValue);
    gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    gl->GenFramebuffers(1, &gfx->terrainCoverageFBO);
    gl->BindFramebuffer(GL_FRAMEBUFFER, gfx->terrainCoverageFBO);

    gl->GenTextures(1, &gfx->terrainCoverageTexture);
    gl->BindTexture(GL_TEXTURE_2D, gfx->terrainCoverageTexture);
    gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    gl->TexImage2D(
        GL_TEXTURE_2D, 0, GL_R8,
        gfx->coverageBufferWidth, gfx->coverageBufferHeight, 0,
        GL_RED, GL_UNSIGNED_BYTE, nullptr
    );
    gl->FramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gfx->terrainCoverageTexture, 0);

    gl->GenTextures(1, &gfx->terrainGraphicsTexture);
    gl->BindTexture(GL_TEXTURE_2D, gfx->terrainGraphicsTexture);
    gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    int width, height;
    unsigned char *pixels = stbi_load("\\liero\\run_tree\\testimg.png", &width, &height, nullptr, 3);
    assert (pixels);
    gl->TexImage2D(
        GL_TEXTURE_2D, 0, GL_RGB,
        width, height, 0,
        GL_RGB, GL_UNSIGNED_BYTE, pixels
    );
    stbi_image_free(pixels);

    createShader(gl, GL_FRAGMENT_SHADER, &gfx->terrainGraphicsFS, terrainGraphicsFSSource);

    gfx->terrainGraphicsProgram = gl->CreateProgram();
    gl->AttachShader(gfx->terrainGraphicsProgram, gfx->simpleVS);
    gl->AttachShader(gfx->terrainGraphicsProgram, gfx->terrainGraphicsFS);
    gl->BindFragDataLocation(gfx->terrainGraphicsProgram, 0, "output_color");
    gl->LinkProgram(gfx->terrainGraphicsProgram);
}

void destroyGfxState(GL *gl, GfxState *gfx) {
    free(gfx->terrainValueTextureSubImageBuffer);

    gl->DeleteProgram(gfx->terrainGraphicsProgram);
    gl->DeleteShader(gfx->terrainGraphicsFS);
    gl->DeleteTextures(1, &gfx->terrainGraphicsTexture);
    gl->DeleteTextures(1, &gfx->terrainCoverageTexture);
    gl->DeleteFramebuffers(1, &gfx->terrainCoverageFBO);
    gl->DeleteTextures(1, &gfx->terrainValueTexture);
    gl->DeleteProgram(gfx->terrainCoverageProgram);
    gl->DeleteShader(gfx->terrainCoverageFS);
    gl->DeleteShader(gfx->simpleVS);
    gl->DeleteBuffers(1, &gfx->immediateVBO);
    gl->DeleteVertexArrays(1, &gfx->openglSucksVAO);
}

void immClear(GfxState *gfx) {
    gfx->immediateCount = 0;
}

void immVertex(GfxState *gfx, Vertex vert) {
    if (gfx->immediateCount + 1 >= gfx->immediateCapacity) {
        gfx->immediateCapacity *= 2;
        gfx->immediateBuffer = (Vertex *) xrealloc(gfx->immediateBuffer, gfx->immediateCapacity * sizeof(Vertex));
    }
    gfx->immediateBuffer[gfx->immediateCount] = vert;
    ++gfx->immediateCount;
}

int immFlush(GL *gl, GfxState *gfx) {
    if (gfx->immediateCount * sizeof(Vertex) > immediateVBOCapacity) {
        /* @FixMe @ErrorHandling */
        fprintf(stderr, "immediate mode buffer overflow\n");
    }
    gl->BindVertexArray(gfx->openglSucksVAO);
    gl->BindBuffer(GL_ARRAY_BUFFER, gfx->immediateVBO);
    gl->BufferSubData(GL_ARRAY_BUFFER, 0, gfx->immediateCount * sizeof(Vertex), &gfx->immediateBuffer[0]);
    return gfx->immediateCount;
}

void immSetColor(GfxState *gfx, unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
    gfx->immediateColor[0] = r;
    gfx->immediateColor[1] = g;
    gfx->immediateColor[2] = b;
    gfx->immediateColor[3] = a;
}

void immSetColor(GfxState *gfx, unsigned char r, unsigned char g, unsigned char b) {
    immSetColor(gfx, r, g, b, 255);
}

void immEdge(GfxState *gfx, float x0, float y0, float x1, float y1) {
    Vertex a, b;

    a.r = b.r = gfx->immediateColor[0];
    a.g = b.g = gfx->immediateColor[1];
    a.b = b.b = gfx->immediateColor[2];
    a.a = b.a = gfx->immediateColor[3];

    a.z = b.z = 0;

    a.x = x0; a.y = y0;
    b.x = x1; b.y = y1;

    a.u = a.v = b.u = b.v = 0;

    immVertex(gfx, a);
    immVertex(gfx, b);
}

void immQuad(GfxState *gfx, float x, float y, float hw, float hh, float z) {
    Vertex a, b, c, d;

    a.r = b.r = c.r = d.r = gfx->immediateColor[0];
    a.g = b.g = c.g = d.g = gfx->immediateColor[1];
    a.b = b.b = c.b = d.b = gfx->immediateColor[2];
    a.a = b.a = c.a = d.a = gfx->immediateColor[3];

    a.z = b.z = c.z = d.z = z;

    a.x = x - hw; a.y = y - hh;
    b.x = x - hw; b.y = y + hh;
    c.x = x + hw; c.y = y + hh;
    d.x = x + hw; d.y = y - hh;

    a.u = a.v = 0;
    b.u = 0; b.v = 1;
    c.u = c.v = 1;
    d.u = 1; d.v = 0;

    immVertex(gfx, c); immVertex(gfx, b); immVertex(gfx, a);
    immVertex(gfx, a); immVertex(gfx, d); immVertex(gfx, c);
}

void immQuad(GfxState *gfx, float x, float y, float hw, float hh) {
    immQuad(gfx, x, y, hw, hh, 0);
}

void immTerrainQuad(GfxState *gfx, int width, int height) {
    Vertex a, b, c, d;
    a.r = b.r = c.r = d.r = a.g = b.g = c.g = d.g = a.b = b.b = c.b = d.b = a.a = b.a = c.a = d.a = 255;
    a.z = b.z = c.z = d.z = 0;

    a.x = 0;             a.y = 0;
    b.x = 0;             b.y = (float) height;
    c.x = (float) width; c.y = (float) height;
    d.x = (float) width; d.y = 0;

    // @FixMe dimensions keep being off by one
    a.u = 0.5f/width;         a.v = 0.5f/height;
    b.u = 0.5f/width;         b.v = (height+0.5f)/height;
    c.u = (width+0.5f)/width; c.v = (height+0.5f)/height;
    d.u = (width+0.5f)/width; d.v = 0.5f/height;

    immVertex(gfx, c); immVertex(gfx, b); immVertex(gfx, a);
    immVertex(gfx, a); immVertex(gfx, d); immVertex(gfx, c);
}

void immShittyText(GfxState *gfx, float x, float y, float scale, const char *text) {
    const int bufferCapacity = 32 * 1024;
    static struct {
        float x, y, z;
        unsigned char r, g, b, a;
    } vertexBuffer[bufferCapacity];
    // @ToDo @Hack figure out why the text parameter isn't const
    int quads = stb_easy_font_print(
        0, 0, (char *) text, gfx->immediateColor,
        &vertexBuffer[0], bufferCapacity * sizeof(vertexBuffer[0])
    );
    for (int i = 0; i < quads; ++i) {
        for (int j = 0; j < 6; ++j) {
            int k = j;
            if (j >= 3) k = (j - 1) % 4;

            Vertex vert;
            vert.x = vertexBuffer[i*4+k].x * scale + x;
            vert.y = -vertexBuffer[i*4+k].y * scale + y;
            vert.z = vertexBuffer[i*4+k].z;
            vert.r = vertexBuffer[i*4+k].r;
            vert.g = vertexBuffer[i*4+k].g;
            vert.b = vertexBuffer[i*4+k].b;
            vert.a = vertexBuffer[i*4+k].a;
            immVertex(gfx, vert);
        }
    }
}

void immCGeo(GfxState *gfx, CollisionGeometry *cgeo, bool normals) {
    for (int i = 0; i < cgeo->count; ++i) {
        Edge edge = cgeo->elements[i];
        immSetColor(gfx, 0, 255, 0);
        immEdge(gfx, edge.lhs.x, edge.lhs.y, edge.rhs.x, edge.rhs.y);
        if (normals) {
            float mx = (edge.lhs.x + edge.rhs.x) / 2;
            float my = (edge.lhs.y + edge.rhs.y) / 2;
            float nx = edge.rhs.y - edge.lhs.y;
            float ny = -(edge.rhs.x - edge.lhs.x);
            float nlen = sqrtf(nx * nx + ny * ny);
            nx /= nlen;
            ny /= nlen;
            immSetColor(gfx, 255, 255, 0);
            immEdge(gfx, mx, my, mx + nx, my + ny);
        }
    }
}

void gfxUploadTerrain(GL *gl, GfxState *gfx, Terrain *terrain) {
    gl->BindTexture(GL_TEXTURE_2D, gfx->terrainValueTexture);
    gl->TexImage2D(
        GL_TEXTURE_2D, 0, GL_R32F,
        terrain->widthTexels, terrain->heightTexels, 0,
        GL_RED, GL_FLOAT, terrain->texels
    );
}

void gfxBlitChunk(GfxState *gfx, Terrain *terrain, int cx, int cy) {
    for (int y = 0; y < chunkSize; ++y) {
        int gy = cy * chunkSize + y;
        int gxBegin = cx * chunkSize;
        memcpy(
            &gfx->terrainValueTextureSubImageBuffer[y * chunkSize],
            &terrain->texels[gxBegin + gy * terrain->widthTexels],
            chunkSize * sizeof(float)
        );
    }
}

void gfxUpdateTerrain(GL *gl, GfxState *gfx, Terrain *terrain) {
    gl->BindTexture(GL_TEXTURE_2D, gfx->terrainValueTexture);
    for (int y = 0; y < terrain->heightChunks; ++y) {
        for (int x = 0; x < terrain->widthChunks; ++x) {
            if (terrain->chunks[x + y * terrain->widthChunks] & CHUNK_BIT_DIRTY_GPU) {
                gfxBlitChunk(gfx, terrain, x, y);
                gl->TexSubImage2D(
                    GL_TEXTURE_2D, 0,
                    x * chunkSize, y * chunkSize, chunkSize, chunkSize,
                    GL_RED, GL_FLOAT, gfx->terrainValueTextureSubImageBuffer
                );
                terrain->chunks[x + y * terrain->widthChunks] &= ~CHUNK_BIT_DIRTY_GPU;
            }
        }
    }
}

void gfxDrawTerrain(GL *gl, GfxState *gfx) {
    float k = 9.0f / 16.0f;
    float l = 1.0f / 64.0f;
    float xterrain[] = {
        k*l, 0, 0, 0,
        0, -l, 0, 0,
        0, 0, 1, 0,
        -k*1, 1, 0, 1,
    };

    gl->BindFramebuffer(GL_FRAMEBUFFER, gfx->terrainCoverageFBO);
    gl->Viewport(0, 0, gfx->coverageBufferWidth, gfx->coverageBufferHeight);
    gl->ClearColor(0, 0, 0, 0);
    gl->Clear(GL_COLOR_BUFFER_BIT);

    immClear(gfx);
    immTerrainQuad(gfx, 128, 128);
    int vertCount = immFlush(gl, gfx);
    vtxEnableAttribs(gl);

    gl->ActiveTexture(GL_TEXTURE0);
    gl->BindTexture(GL_TEXTURE_2D, gfx->terrainValueTexture);

    gl->UseProgram(gfx->terrainCoverageProgram);
    gl->UniformMatrix4fv(0, 1, GL_FALSE, &xterrain[0]);
    gl->Uniform1i(1, 0);

    gl->DrawArrays(GL_TRIANGLES, 0, vertCount);

    gl->BindFramebuffer(GL_FRAMEBUFFER, 0);
    gl->Viewport(0, 0, gfx->backbufferWidth, gfx->backbufferHeight);
    gl->ClearColor(100.f/255, 149.f/255, 237.f/255, 1);
    gl->Clear(GL_COLOR_BUFFER_BIT);

    immClear(gfx);
    immQuad(gfx, 0, 0, 1, 1);
    vertCount = immFlush(gl, gfx);

    gl->ActiveTexture(GL_TEXTURE0);
    gl->BindTexture(GL_TEXTURE_2D, gfx->terrainGraphicsTexture);
    gl->ActiveTexture(GL_TEXTURE1);
    gl->BindTexture(GL_TEXTURE_2D, gfx->terrainCoverageTexture);

    gl->UseProgram(gfx->terrainGraphicsProgram);
    float identity[] = {
        1, 0, 0, 0,
        0, -1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1,
    };
    gl->UniformMatrix4fv(0, 1, GL_FALSE, &identity[0]);
    gl->Uniform1i(1, 0);
    gl->Uniform1i(2, 1);
    gl->Uniform2i(3, gfx->backbufferWidth, gfx->backbufferHeight);

    gl->Enable(GL_BLEND);
    gl->BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    gl->DrawArrays(GL_TRIANGLES, 0, vertCount);
    gl->Disable(GL_BLEND);

    vtxDisableAttribs(gl);
}

void gfxDebugDrawTerrain(GL *gl, GfxState *gfx, Terrain *terrain) {
    float k = 9.0f / 16.0f;
    float l = 1.0f / 64.0f;
    float xterrain[] = {
        k*l, 0, 0, 0,
        0, -l, 0, 0,
        0, 0, 1, 0,
        -k*1, 1, 0, 1,
    };

    immClear(gfx);
    immSetColor(gfx, 0, 255, 0);
    for (int i = 0; i < terrain->widthChunks * terrain->heightChunks; ++i) {
        immCGeo(gfx, &terrain->collisionGeometries[i]);
    }
    int vertCount = immFlush(gl, gfx);
    vtxEnableAttribs(gl);
    gl->UseProgram(gfx->simpleProgram);
    gl->UniformMatrix4fv(0, 1, GL_FALSE, &xterrain[0]);
    gl->DrawArrays(GL_LINES, 0, vertCount);
    vtxDisableAttribs(gl);
}
