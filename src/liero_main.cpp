/* Copyright 2017, Stijn Brouwer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "liero_main.h"
#include "liero_net.h"
#include "liero_opengl.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <SDL.h>
#undef main
#include <enet/enet.h>

const char *usage = R"(Usage:
For clients:
    liero client (x resolution) (y resolution) (server address) (server port)
For servers:
    liero server (address to bind to) (port number)
)";


void printUsage() {
    fputs(usage, stderr);
}

int main(int argc, char **argv) {
    if (argc < 2) {
        fputs("Error: wrong number of CLI arguments\n", stderr);
        printUsage();
        return EXIT_FAILURE;
    }
    if (0 == strcmp("client", argv[1])) {
        if (argc != 6) {
            fputs("Error: wrong number of CLI arguments for a client\n", stderr);
            printUsage();
            return EXIT_FAILURE;
        }
        int width = atoi(argv[2]),
            height = atoi(argv[3]);
        const char *address = argv[4];
        int port = atoi(argv[5]);
        clientEntryPoint(width, height, address, port);
        return EXIT_SUCCESS;
    }
    if (0 == strcmp("server", argv[1])) {
        if (argc != 4) {
            fputs("Error: wrong number of CLI arguments for a server\n", stderr);
            printUsage();
            return EXIT_FAILURE;
        }
        const char *address = argv[2];
        int port = atoi(argv[3]);
        serverEntryPoint(address, port);
        return EXIT_SUCCESS;
    }
    fputs("Error: expected either \"client\" or \"server\" as the first CLI argument\n", stderr);
    printUsage();
    return EXIT_FAILURE;
}

void serverEntryPoint(const char *addressStr, int port) {
    ServerData data;
    createTestTerrain(&data.terrain);
    createStack(&data.queuedTerrainUpdates);

    if (enet_initialize() != 0) {
        fputs("Failed to initialize ENet\n", stderr);
        return;
    }

    ENetAddress address;
    enet_address_set_host(&address, addressStr);
    address.port = port;
    ENetHost *server = enet_host_create(&address, 32, 1, 0, 0);

    if (server == nullptr) {
        fputs("Failed to create server host\n", stderr);
        return;
    }

    ENetEvent event;
    while (true) {
        enet_host_service(server, &event, 1000);
        if (event.type == ENET_EVENT_TYPE_CONNECT) {
            printf("A new client connected from %x:%u.\n", event.peer->address.host, event.peer->address.port);
        }
        if (event.type == ENET_EVENT_TYPE_DISCONNECT) {
            printf("Client at %x:%u disconnected.\n", event.peer->address.host, event.peer->address.port);
        }
        if (event.type == ENET_EVENT_TYPE_RECEIVE) {
            auto kind = (PacketKind *) event.packet->data;
            if (*kind == PACKET_KIND_TERRAIN_MAKE_HOLE) {
                auto payload = (TerrainMakeHolePayload *) (kind + 1);
                printf("Got message to make a hole at %f, %f of radius %f\n", payload->x, payload->y, payload->radius);
                applyTerrainMakeHole(&data, payload);
                for (int i = 0; i < data.queuedTerrainUpdates.count; ++i) {
                    /* @ToDo @Speed optimize memory copying into packets */
                    constexpr auto packetSize = sizeof(PacketKind) + sizeof(TerrainUpdatePayload);
                    auto packet = stackalloc(packetSize);
                    auto kind = (int *) &packet;
                    *kind = PACKET_KIND_TERRAIN_UPDATE;
                    auto payload = (TerrainUpdatePayload *) (kind + 1);
                    memcpy(payload, &data.queuedTerrainUpdates.elements[i], sizeof(TerrainUpdatePayload));
                    ENetPacket *updatePacket = enet_packet_create(&packet, packetSize, ENET_PACKET_FLAG_RELIABLE);
                    enet_host_broadcast(server, 0, updatePacket);
                }
                clear(&data.queuedTerrainUpdates);
                enet_host_flush(server);
                printf("Sent the newly updated chunks back to the clients\n");
            }
            else {
                fprintf(stderr, "Unknown packet kind %x\n", *kind);
            }
            enet_packet_destroy(event.packet);
        }
    }

    destroyTerrain(&data.terrain);
    destroyStack(&data.queuedTerrainUpdates);
    enet_host_destroy(server);
    enet_deinitialize();
}

void APIENTRY debugMessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity,
                                   GLsizei length, const GLchar *message, const void *userParam)
{
    // @ToDo logging
    fprintf(stderr, "OpenGL Debug Message: %s\n", message);
}

struct Platform {
    SDL_Window *window;
    SDL_GLContext glContext;
    GL gl;
};

void initPlatform(Platform *plat, int width, int height) {
    /* Although we don't use the SDL_Render API, SDL_INIT_VIDEO is needed for the context to work properly in RenderDoc */
    SDL_Init(SDL_INIT_VIDEO);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    plat->window = SDL_CreateWindow(
        "liero clone",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        width, height,
        SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN
    );

    plat->glContext = SDL_GL_CreateContext(plat->window);
    SDL_GL_MakeCurrent(plat->window, plat->glContext);

    loadGL(&plat->gl);

    plat->gl.Enable(GL_DEBUG_OUTPUT);
    plat->gl.Enable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    plat->gl.DebugMessageCallback(debugMessageCallback, nullptr);
}

void quitPlatform(Platform *plat) {
    SDL_GL_DeleteContext(plat->glContext);
    SDL_DestroyWindow(plat->window);
}

void clientEntryPoint(int width, int height, const char *addressStr, int port) {
    if (enet_initialize() != 0) {
        fputs("Failed to initialize ENet\n", stderr);
        return;
    }

    ENetHost *client = enet_host_create(nullptr, 1, 1, 0, 0);

    if (client == nullptr) {
        fputs("Failed to create client host\n", stderr);
        return;
    }

    ENetAddress address;
    enet_address_set_host(&address, addressStr);
    address.port = port;
    ENetPeer *server = enet_host_connect(client, &address, 2, 0);

    if (server == nullptr) {
        fputs("Failed to create a connection to the server\n", stderr);
        return;
    }

    ENetEvent event;
    if (enet_host_service(client, &event, 5000) > 0 && event.type == ENET_EVENT_TYPE_CONNECT) {
        fputs("Connected to server!\n", stderr);
    }
    else {
        fputs("Failed to establish a connection with the server\n", stderr);
        return;
    }
    enet_host_flush(client);

    Platform platform;
    initPlatform(&platform, width, height);

    GfxState gfx = {};
    createGfxState(&platform.gl, &gfx, width, height);

    ClientData data;
    createTestTerrain(&data.terrain);

    gfxUploadTerrain(&platform.gl, &gfx, &data.terrain);

    while (true) {
        SDL_Event ev;
        while (SDL_PollEvent(&ev)) {
            if (ev.type == SDL_QUIT) {
                goto end;
            }
            if (ev.type == SDL_MOUSEBUTTONDOWN) {
                if (ev.button.button != SDL_BUTTON_LEFT) continue;
                float x = (float) ev.button.x, y = (float) ev.button.y;
                float zoom = 128;
                y = y / height * zoom;
                x = (x + (height - width) / 2) / height * zoom;
                printf("Put hole at %f, %f\n", x, y);
                constexpr auto packetSize = sizeof(PacketKind) + sizeof(TerrainMakeHolePayload);
                auto packet = stackalloc(packetSize);
                auto kind = (int *) &packet;
                *kind = PACKET_KIND_TERRAIN_MAKE_HOLE;
                auto payload = (TerrainMakeHolePayload *) (kind + 1);
                payload->x = x;
                payload->y = y;
                payload->radius = 8;
                ENetPacket *holePacket = enet_packet_create(&packet, packetSize, ENET_PACKET_FLAG_RELIABLE);
                enet_peer_send(server, 0, holePacket);
                enet_host_flush(client);
            }
        }

        ENetEvent serverEvent;
        while (enet_host_service(client, &serverEvent, 0) > 0) {
            if (serverEvent.type == ENET_EVENT_TYPE_RECEIVE) {
                auto kind = (PacketKind *) serverEvent.packet->data;
                if (*kind == PACKET_KIND_TERRAIN_UPDATE) {
                    auto update = (TerrainUpdatePayload *) (kind + 1);
                    printf("Received command to update chunk at %d, %d\n", update->chunkX, update->chunkY);
                    applyTerrainUpdate(&data, update);
                }
                else {
                    fprintf(stderr, "Unknown packet type %x\n", *kind);
                }
                enet_packet_destroy(serverEvent.packet);
            }
        }

        //gfxUploadTerrain(&platform.gl, &gfx, &data.terrain);
        gfxUpdateTerrain(&platform.gl, &gfx, &data.terrain);
        terrainUpdateCGeos(&data.terrain);

        gfxDrawTerrain(&platform.gl, &gfx);
        gfxDebugDrawTerrain(&platform.gl, &gfx, &data.terrain);

        SDL_GL_SwapWindow(platform.window);
    }

end:
    enet_peer_disconnect(server, 0);
    enet_host_flush(client);

    enet_host_destroy(client);
    enet_deinitialize();

    destroyTerrain(&data.terrain);
    quitPlatform(&platform);
}
