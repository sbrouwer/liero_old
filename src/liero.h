/* Copyright 2017, Stijn Brouwer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LIERO_H_GUARD_
#define __LIERO_H_GUARD_

#include <stdlib.h>

void die(const char *fmt, ...);
#define DIE_OOM() die("Allocation failed: Out of memory")

void *xmalloc(size_t size);
void *xzalloc(size_t size);
void *xrealloc(void *ptr, size_t size);

template <size_t size>
struct StackAlloc {
    char bytes[size];
};

/** Safer replacement for alloca() with compile-time constant arguments */
#define stackalloc(size) (StackAlloc<(size)>())

template <typename T>
struct Stack {
    int capacity, count;
    T *elements;
};

template <typename T>
void createStack(Stack<T> *stack, int initialCapacity = 0) {
    stack->capacity = initialCapacity;
    stack->count = 0;
    if (initialCapacity != 0) {
        stack->elements = (T *) xmalloc(initialCapacity * sizeof(T));
    } else {
        stack->elements = nullptr;
    }
}

template <typename T>
void destroyStack(Stack<T> *stack) {
    if (stack->elements != nullptr) {
        free(stack->elements);
    }
}

template <typename T>
void push(Stack<T> *stack, T element) {
    if (stack->count + 1 >= stack->capacity) {
        if (stack->capacity == 0) {
            stack->capacity = 1;
        } else {
            stack->capacity *= 2;
        }
        stack->elements = (T *) xrealloc(stack->elements, stack->capacity * sizeof(T));
    }
    stack->elements[stack->count] = element;
    ++stack->count;
}

template <typename T>
void clear(Stack<T> *stack) {
    stack->count = 0;
}

struct Vector2 {
    float x, y;

    inline Vector2(float x, float y) : x(x), y(y) { }
};

struct Edge {
    /** Normal is up relative to the left and right sides */
    Vector2 lhs, rhs;

    inline Edge(Vector2 l, Vector2 r) : lhs(l), rhs(r) { }
};

constexpr int collisionGeometryDefaultCapacity = 64;

/**
 * Edge-based geometry for collision detection and response in the game's physics
 * This is generated using the marching squares algorithm (see marchSquares).
 */
typedef Stack<Edge> CollisionGeometry;

/** Chunk size in texels */
constexpr int chunkSize = 16;

typedef int ChunkBits;
enum {
    /** Collision geometry needs to be regenerated for this chunk */
    CHUNK_BIT_DIRTY_COLLISION = 0b0001,
    /** This chunk needs to be updated in the GPU texture */
    CHUNK_BIT_DIRTY_GPU = 0b0010,

    CHUNK_BITS_DIRTY = CHUNK_BIT_DIRTY_COLLISION | CHUNK_BIT_DIRTY_GPU,
};

/**
 * The default value of the texels in Terrain.texels.
 * Our terrain is a signed distance field, but to preserve locality, we pick a maximum distance
 * (which functions as a cap on the area of influence of operations on the field).
 */
constexpr float terrainMaxDistance = (float) chunkSize / 2;

/**
 * The value for the isocurve we want to extract from Terrain.texels.
 * Anything greater than or equal to this is considered solid terrain.
 */
constexpr float terrainIsovalue = 0.0f;

struct Terrain {
    /** Dimensions of the terrain in texels. Used for accessing the texel array. */
    int widthTexels, heightTexels;
    /** Source values that are compared to terrainIsovalue in order to create the terrain's shape */
    float *texels;
    /**
     * Dimensions of the terrain in chunks. Used for accessing chunks and cgeos.
     * The texels are generally stored in power-of-two dimensions,
     * but because there are only (width - 1)(height - 1) squares to march,
     * the outer chunks don't have enough texels to be fully filled.
     * To fix this we just ignore the outermost chunks,
     * this effectively means that dimChunks = (dim - 1) / chunkSize.
     */
    int widthChunks, heightChunks;
    /** Miscellaneous bitflags associated with chunks */
    ChunkBits *chunks;
    /** Collision geometries associated with the chunks */
    CollisionGeometry *collisionGeometries;
};

/** Releases all (clientside) memory used by a terrain instance */
void destroyTerrain(Terrain *terrain);

/** Update the collision geometry of the given chunk */
void marchSquares(Terrain *terrain, int chunkX, int chunkY);
/** Updates all chunks with stale collision geometry */
void terrainUpdateCGeos(Terrain *terrain);

void createTestTerrain(Terrain *terrain);

#endif
