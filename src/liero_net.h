/* Copyright 2017, Stijn Brouwer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LIERO_NET_H_GUARD_
#define __LIERO_NET_H_GUARD_

#include "liero.h"
#include <enet/enet.h>

typedef int PacketKind;
enum {
    PACKET_KIND_TERRAIN_UPDATE,
    PACKET_KIND_TERRAIN_MAKE_HOLE,
};

struct TerrainUpdatePayload {
    int chunkX, chunkY;
    float texels[chunkSize * chunkSize];
};

struct TerrainMakeHolePayload {
    float x, y;
    float radius;
};

struct ServerData {
    Terrain terrain;

    Stack<TerrainUpdatePayload> queuedTerrainUpdates;
};

struct ClientData {
    Terrain terrain;

    Stack<TerrainMakeHolePayload> queuedHoles;
};

void applyTerrainMakeHole(ServerData *data, TerrainMakeHolePayload *hole);

void applyTerrainUpdate(ClientData *data, TerrainUpdatePayload *update);

#endif
